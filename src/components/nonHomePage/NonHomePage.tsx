import {Component} from 'react';
import { Link } from "react-router-dom";

function NonHomePage(wrappedComponent: Component) {
  return (
  <div className="NonHomePageContents">
  <Link to="/" className="LogoText ShadowedText">electoralist.org</Link>
  {wrappedComponent}
  </div>);
}

export default NonHomePage;