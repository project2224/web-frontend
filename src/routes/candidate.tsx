import ChromeWrapper from '../ChromeWrapper.tsx';
import Candidate from '../pages/candidate/Candidate.tsx';

function CandidateRoute({match, location}) {
    document.title = "electoralist.org Candidate";
    console.log(location);
    return <ChromeWrapper
        wrappedComponent={Candidate()}
        currentPage={null}
    />;
}

export default CandidateRoute;
