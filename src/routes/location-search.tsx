import ChromeWrapper from '../ChromeWrapper.tsx';
import LocationSearch from '../pages/locationSearch/LocationSearch.tsx';
import {useSearchParams} from "react-router-dom";

function LocationSearchRoute() {

    document.title = "electoralist.org Location Search";
    return <ChromeWrapper
        wrappedComponent={LocationSearch()}
        currentPage={null}
    />;
}

export default LocationSearchRoute;
